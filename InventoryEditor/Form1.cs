﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryManger
{
    public partial class Form1 : Form
    {
        private int Indexnum = 0;
        private List<Bags> _bags = new List<Bags>();

        public Form1()
        {
            InitializeComponent();

            if (File.Exists("temp.bin"))
            {
                List<Bags> tempBagses = DataManger.LoadData<List<Bags>>("temp.bin");
                Indexnum = tempBagses.Count;

                foreach (var bag in tempBagses)
                {
                    _bags.Add(bag);
                    BagnameListbox.Items.Add(bag.BagName);
                }
            }
            else
            {
                File.Create("temp.bin").Dispose();
            }
        }

        private void AddBagButton_Click(object sender, EventArgs e)
        {
            _bags.Add(new Bags($"#{Indexnum}", Indexnum, new List<Bagsitem>()));
            BagnameListbox.Items.Add(_bags[Indexnum].BagName);
            Indexnum++;
        }

        private void BagnameListbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_bags.Count != -1 && BagnameListbox.Items.Count != -1 && BagnameListbox.SelectedIndex != -1)
            {
                var bag = _bags[BagnameListbox.SelectedIndex];

                Bagiitemlistbox.Items.Clear();
                Tagslistbox.Items.Clear();

                if(bag.BagitemsList == null)
                    return;

                foreach (var bagitem in bag.BagitemsList)
                {
                    Bagiitemlistbox.Items.Add(bagitem.Itemname);

                    if (bagitem.Tags != null)
                    {
                        foreach (var tag in bagitem.Tags)
                        {
                            Tagslistbox.Items.Add(tag);
                        }
                    }
                }
            }
            else
            {
                Bagiitemlistbox.Items.Clear();
                Tagslistbox.Items.Clear();
            }
        }

        private void Bagiitemlistbox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Bagiitemlistbox.SelectedIndex != -1 && BagnameListbox.SelectedIndex != -1)
            {
                var bag = _bags[BagnameListbox.SelectedIndex];
                Tagslistbox.Items.Clear();

                foreach (var tag in bag.BagitemsList[Bagiitemlistbox.SelectedIndex].Tags)
                {
                    Tagslistbox.Items.Add(tag);
                }
            }
        }

        private void AddTagButton_Click(object sender, EventArgs e)
        {
            if (Bagiitemlistbox.SelectedIndex != -1 && !string.IsNullOrEmpty(AddTagsTextBox.Text))
            {
                var Tags = _bags[BagnameListbox.SelectedIndex].BagitemsList[Bagiitemlistbox.SelectedIndex].Tags;
                bool addtolist = true;

                foreach (var tag in Tags)
                {
                    if (tag.ToLower().Equals(AddTagsTextBox.Text.ToLower()))
                    {
                        addtolist = false;
                    }
                }

                if (addtolist)
                {
                    Tags.Add(AddTagsTextBox.Text);
                    Tagslistbox.Items.Add(AddTagsTextBox.Text);
                }
            }

            AddTagsTextBox.Text = string.Empty;
        }

        private void AdditemButton_Click(object sender, EventArgs e)
        {
            if (BagnameListbox.SelectedIndex != -1 && !string.IsNullOrEmpty(Additemtextbox.Text))
            {
                var itemsBagsitems = _bags[BagnameListbox.SelectedIndex].BagitemsList;

                bool addtolist = true;
                foreach (var item in itemsBagsitems)
                {
                    if (item.Itemname.ToLower().Equals(Additemtextbox.Text.ToLower()))
                    {
                        addtolist = false;
                    }
                }

                if (addtolist)
                {
                    itemsBagsitems.Add(new Bagsitem(Additemtextbox.Text, new List<string>()));
                    Bagiitemlistbox.Items.Add(Additemtextbox.Text);
                }
            }

            Additemtextbox.Text = string.Empty;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DataManger.SaveData(_bags, "temp.bin");
        }
    }
}
