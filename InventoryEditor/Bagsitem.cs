﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace InventoryManger
{
    [ProtoContract]
    public class Bagsitem
    {
        [ProtoMember(1)]
        public string Itemname { get; set; }
        [ProtoMember(2)]
        public DateTime Dateadded { get; set; }
        [ProtoMember(3)]
        public List<string> Tags;

        public Bagsitem()
        {

        }

        public Bagsitem(string _itemname, List<string> _tags)
        {
            this.Itemname = _itemname;
            this.Dateadded = DateTime.Now;
            this.Tags = new List<string>(_tags);
        }
    }
}