﻿namespace InventoryManger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BagnameListbox = new System.Windows.Forms.ListBox();
            this.AddBagButton = new System.Windows.Forms.Button();
            this.Bagiitemlistbox = new System.Windows.Forms.ListBox();
            this.Additemtextbox = new System.Windows.Forms.TextBox();
            this.Tagslistbox = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AddTagButton = new System.Windows.Forms.Button();
            this.AddTagsTextBox = new System.Windows.Forms.TextBox();
            this.AdditemButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BagnameListbox
            // 
            this.BagnameListbox.FormattingEnabled = true;
            this.BagnameListbox.Location = new System.Drawing.Point(12, 38);
            this.BagnameListbox.Name = "BagnameListbox";
            this.BagnameListbox.Size = new System.Drawing.Size(161, 446);
            this.BagnameListbox.TabIndex = 1;
            this.BagnameListbox.SelectedIndexChanged += new System.EventHandler(this.BagnameListbox_SelectedIndexChanged);
            // 
            // AddBagButton
            // 
            this.AddBagButton.Location = new System.Drawing.Point(12, 12);
            this.AddBagButton.Name = "AddBagButton";
            this.AddBagButton.Size = new System.Drawing.Size(161, 23);
            this.AddBagButton.TabIndex = 2;
            this.AddBagButton.Text = "Add Bag";
            this.AddBagButton.UseVisualStyleBackColor = true;
            this.AddBagButton.Click += new System.EventHandler(this.AddBagButton_Click);
            // 
            // Bagiitemlistbox
            // 
            this.Bagiitemlistbox.FormattingEnabled = true;
            this.Bagiitemlistbox.Location = new System.Drawing.Point(362, 38);
            this.Bagiitemlistbox.Name = "Bagiitemlistbox";
            this.Bagiitemlistbox.Size = new System.Drawing.Size(284, 446);
            this.Bagiitemlistbox.TabIndex = 3;
            this.Bagiitemlistbox.SelectedIndexChanged += new System.EventHandler(this.Bagiitemlistbox_SelectedIndexChanged);
            // 
            // Additemtextbox
            // 
            this.Additemtextbox.Location = new System.Drawing.Point(362, 12);
            this.Additemtextbox.Name = "Additemtextbox";
            this.Additemtextbox.Size = new System.Drawing.Size(204, 20);
            this.Additemtextbox.TabIndex = 4;
            // 
            // Tagslistbox
            // 
            this.Tagslistbox.FormattingEnabled = true;
            this.Tagslistbox.Location = new System.Drawing.Point(6, 45);
            this.Tagslistbox.Name = "Tagslistbox";
            this.Tagslistbox.Size = new System.Drawing.Size(294, 420);
            this.Tagslistbox.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AddTagButton);
            this.groupBox1.Controls.Add(this.AddTagsTextBox);
            this.groupBox1.Controls.Add(this.Tagslistbox);
            this.groupBox1.Location = new System.Drawing.Point(823, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(306, 472);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tags";
            // 
            // AddTagButton
            // 
            this.AddTagButton.Location = new System.Drawing.Point(218, 17);
            this.AddTagButton.Name = "AddTagButton";
            this.AddTagButton.Size = new System.Drawing.Size(75, 23);
            this.AddTagButton.TabIndex = 7;
            this.AddTagButton.Text = "Add Tag";
            this.AddTagButton.UseVisualStyleBackColor = true;
            this.AddTagButton.Click += new System.EventHandler(this.AddTagButton_Click);
            // 
            // AddTagsTextBox
            // 
            this.AddTagsTextBox.Location = new System.Drawing.Point(6, 19);
            this.AddTagsTextBox.Name = "AddTagsTextBox";
            this.AddTagsTextBox.Size = new System.Drawing.Size(206, 20);
            this.AddTagsTextBox.TabIndex = 6;
            // 
            // AdditemButton
            // 
            this.AdditemButton.Location = new System.Drawing.Point(571, 9);
            this.AdditemButton.Name = "AdditemButton";
            this.AdditemButton.Size = new System.Drawing.Size(75, 23);
            this.AdditemButton.TabIndex = 7;
            this.AdditemButton.Text = "Add Item";
            this.AdditemButton.UseVisualStyleBackColor = true;
            this.AdditemButton.Click += new System.EventHandler(this.AdditemButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1161, 500);
            this.Controls.Add(this.AdditemButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Additemtextbox);
            this.Controls.Add(this.Bagiitemlistbox);
            this.Controls.Add(this.AddBagButton);
            this.Controls.Add(this.BagnameListbox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox BagnameListbox;
        private System.Windows.Forms.Button AddBagButton;
        private System.Windows.Forms.ListBox Bagiitemlistbox;
        private System.Windows.Forms.TextBox Additemtextbox;
        private System.Windows.Forms.ListBox Tagslistbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox AddTagsTextBox;
        private System.Windows.Forms.Button AddTagButton;
        private System.Windows.Forms.Button AdditemButton;
    }
}

