﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryManger
{
    public class DataManger
    {
        /// <summary>
        /// Save a list of Clipdata to a location on disk.
        /// </summary>
        /// <param name="data"><summary>List of Clipdata to save.</summary></param>
        /// <param name="path"><summary>Save path.</summary></param>
        public static void SaveData<T>(T data, string path)
        {
            using (var file = File.Create(path))
            {
                Serializer.Serialize(file, data);
            }
        }

        /// <summary>
        /// Load a list of Clipdata from target path.
        /// </summary>
        /// <param name="path"><summary>Save path.</summary></param>
        /// <returns></returns>
        public static T LoadData<T>(string path)
        {
            T loadedData;
            using (var file = File.OpenRead(path))
            {
                loadedData = Serializer.Deserialize<T>(file);
            }
            return loadedData;
        }
    }
}
