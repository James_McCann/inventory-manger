﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace InventoryManger
{
    [ProtoContract]
    public class Bags
    {
        [ProtoMember(1)]
        public DateTime Dateadded { get; set; }
        [ProtoMember(2)]
        public int Id { get; set; }
        [ProtoMember(3)]
        public string BagName { get; set; }
        [ProtoMember(4)]
        public List<Bagsitem> BagitemsList { get; set; }

        public Bags()
        {
           
        }

        public Bags(string _bagName, int _num, List<Bagsitem> _bagitemsList)
        {
            this.Id = _num;
            this.BagName = _bagName;
            this.Dateadded = DateTime.Now;
            this.BagitemsList = new List<Bagsitem>(_bagitemsList);
        }
    }
}
